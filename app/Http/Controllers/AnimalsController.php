<?php

namespace App\Http\Controllers;

use App\Http\Resources\AnimalsResource;
use App\Models\Animals;
use App\Http\Requests\AnimalsRequest;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class AnimalsController extends Controller
{
     /**
     * @var Animals
     */
    protected $modelClass = Animals::class;

    /**
     * @var AnimalsResource
     */
    protected $modelResourceClass = AnimalsResource::class;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  AnimalsResource::collection(Animals::get());
    }

    /**
	 * Store a newly created resource in storage.
	 * @param AnimalsRequest $request
	 * @return Response
	 */
	public function store(AnimalsRequest $request)
	{
        $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        $request->image->move(public_path('images'), $imageName);
        
        $animals = $this->modelClass::create($request->validated());

		return AnimalsResource::make($animals);
	}

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Animals  $animals
     * @return \Illuminate\Http\Response
     */
    public function show(Animals $animals)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Animals  $animals
     * @return \Illuminate\Http\Response
     */
    public function update(AnimalsRequest $request)
    {
        $updated_data = $request->all();
        // dd($updated_data);
		$animals = $this->modelClass::findOrFail($updated_data['id']);

		$animals->update($updated_data);

		return AnimalsResource::make($animals);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Animals  $animals
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $animals = $this->modelClass::findOrFail($request['id']);
        $animals->delete();
        return true;
    }
}
