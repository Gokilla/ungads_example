<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Animals extends Model
{
    public $fillable = [
        'name',
        'family',
        'nickname',
        'photo'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
