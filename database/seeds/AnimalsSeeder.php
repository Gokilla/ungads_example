<?php

use App\Models\Animals;
use Illuminate\Database\Seeder;

class AnimalsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Animals::create([
            'name' => 'Попугай',
            'family' => 'Птицы',
            'nickname' => 'Кеша',
            'photo' => 'placeholder',
            ]);
    }
}
